# Templates for secret files

If the default paths are used, just create a folder in `main`/`..` named `secrets`, which should include sensitive data. Here are some examples, how those files should look.

## `htpasswd`

Generate this file with `htpassw`. This file is used for the http authentication for webdav (via Apache)...


## `nextcloud_db_password.txt` & `nextcloud_db_root_password.txt`

Just a plain text file containing the password


## `onlyoffice.env`

The `JWT_SECRET` see [here](https://github.com/ONLYOFFICE/Docker-DocumentServer#available-configuration-parameters).
