#!/bin/env bash

: "${WEBROOT:=/var/www/acme-challenge}"
: "${TOP_LEVEL_DOMAINS:=commean.eu commean.at commean.de}"
: "${SUBDOMAINS:=www wekan dolibarr frp backend public mattermost}"

echo "Webroot is '${WEBROOT}'"

declare -a domains

eval "tlds=($TOP_LEVEL_DOMAINS)"
eval "subdomains=($SUBDOMAINS)"

for top_level_domain in ${tlds[@]}; do
    domains+=("$top_level_domain")
    for subdomain in ${subdomains[@]}; do
        domains+=("$subdomain.$top_level_domain")
    done
done

echo "Generating certs for the following domains: ${domains[@]}"

if [[ "$(
    read -r -e -p 'Continue? [y/N]> '
    echo "$REPLY"
)" != [Yy]* ]]; then
    echo "Exiting..."
    exit 1
fi


args=""
for domain in ${domains[@]}; do
    args+="-d $domain "
done


docker run  -it --rm --name certbot \
            -v schoberserver_letsencrypt:/etc/letsencrypt \
            -v schoberserver_acme-challenge:"${WEBROOT}" \
            certbot/certbot \
            certonly \
            $args \
            -w "${WEBROOT}"



docker exec schoberserver_nginx_1 nginx -s reload

