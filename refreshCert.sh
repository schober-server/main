#!/bin/bash

docker run  -it --rm --name certbot \
            -v schoberserver_letsencrypt:/etc/letsencrypt \
            -v schoberserver_acme-challenge:/var/www/acme-challenge \
            certbot/certbot \
            --webroot-path=/var/www/acme-challenge \
            renew

docker exec schoberserver_nginx_1 nginx -s reload

