# Main Server Config

This repo includes the main dockerfiles being used by the schober-server

## Setup

To generate the SSL certs only "enable" [httpTOhttps](https://gitlab.com/schober-server/nginx-config/-/blob/2c9b41b19061d838d585ff8f80c09ede44b6ace0/sites-enabled/httpTOhttps.conf) and use the interactive setup for certbot with the following command:


```bash
docker run  -it --rm --name certbot \
            -v schoberserver_letsencrypt:/etc/letsencrypt \
            -v schoberserver_acme-challenge:/var/www/acme-challenge \
            certbot/certbot \
            certonly
```

With nginx as the server, `webroot` and `/var/www/acme-challenge` as the path has to be selected via the interactive configuration for the certbot to succeed.
But before nextcloud can be used, the password files for the db-root/user need to be set.

## Refresh certificates

To refresh the certificates use the following command:

```bash
docker run  -it --rm --name certbot \
            -v schoberserver_letsencrypt:/etc/letsencrypt \
            -v schoberserver_acme-challenge:/var/www/acme-challenge \
            certbot/certbot \
            --webroot-path=/var/www/acme-challenge \
            renew
```

## Setup rclone

To use rclone to backup the files a config has to be generated first. Use this command to configure rclone. The settings are saved in the `rclone-config` volume and can be used to backup the files to a specific location.

```bash
docker run -it --rm --name rclone-setup \
           -v rclone-config:/config/ \
           rclone/rclone config
```

## Architecture
![](https://gitlab.com/schober-server/documentation/images/-/raw/f33d55b6cd68ce78a9a9c6250e1d43a37e5516ab/ArchitectureV02.png)